# Numerical Recall Quest - Memory Game

A fun and challenging memory game built with PyQt5. Click numbered buttons in the right order. Secure victory with a winning streak of three or more to progress to the next level!

[![gif 1](https://s6.gifyu.com/images/S8uZ9.gif)](https://gifyu.com/image/S8uZ9)
[![gif 2](https://s6.gifyu.com/images/S8uZL.gif)](https://gifyu.com/image/S8uZL)
## Features

- Dynamic difficulty levels
- Winning streak tracker
- Color feedback on button clicks
- Assistance on wrong choices
- Victory messages

## How to Play

1. Install PyQt5 (if not already).
2. Run the game with `python main.py`.
3. Keep a winning streak of 3 or more to progress to the next level.

## Author

- Kazi Ar Rafi

## License

GPL-2 License

Enjoy the game!
