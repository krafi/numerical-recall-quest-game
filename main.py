import sys
import random
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt , QTimer
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QGridLayout, QWidget, QMessageBox, QLabel, QSizePolicy, QVBoxLayout
class MemoryGame(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
    def initUI(self):
        self.hide_time = 1000
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        self.layout = QGridLayout()
        self.central_widget.setLayout(self.layout)

        self.setWindowTitle('Numerical Recall Quest game')
        self.setGeometry(100, 100, 400, 400)

        self.buttons = []
        self.start_from = 1

        self.level = 2
        self.winning_streak = 3
        self.createButtons(self.level)


        vertical_layout = QVBoxLayout()
        self.level_label = QLabel(f'', self)
        font = QFont()
        font.setPointSize(16)
        self.level_label.setFont(font)
        self.level_label.setAlignment(Qt.AlignLeft)
        self.level_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.level_label.setFixedWidth(10000)
        vertical_layout.addWidget(self.level_label)

    def createButtons(self, level):
        self.num_buttons = self.level + 1
        button_numbers = random.sample(range(1, self.num_buttons + 1), self.num_buttons)
        font = QFont()
        font.setPointSize(20)

        rows = self.num_buttons // 3 + 1

        row, col = 0, 0
        for number in button_numbers:
            button = QPushButton(str(number), self)
            button.setFont(font)
            button.setStyleSheet("background-color: red; color: green;")
            button.setFixedSize(100, 100)
            button.clicked.connect(self.buttonClicked)
            self.buttons.append(button)
            self.layout.addWidget(button, row, col)
            col += 1
            if col > 2:
                col = 0
                row += 1

        self.hide_time = self.hide_time + (self.level * 10)
        print(self.hide_time)
        timer = QTimer(self)
        timer.singleShot(self.hide_time, self.hideButtonColors)

    def buttonClicked(self):
        sender = self.sender()

        if sender.text() == str(self.start_from):
            sender.setDisabled(True)
            self.start_from += 1
            sender.setStyleSheet("background-color: blue; color: lightblue;")


            if self.start_from == self.num_buttons + 1:
                QMessageBox.information(self, 'You Won!', 'Congratulations! You have won the game.')
                self.winning_streak +=1
                self.level_label.setText(f'{ " " * 4} Level: {self.level} { " " * 15}  Winning Streak: {self.winning_streak} ')

                self.resetGame()

        else:
            #QMessageBox.warning(self, 'Wrong Choice', 'Try again.')
            sender.setStyleSheet("background-color: red; color: lightred;")
            self.winning_streak = 0
            self.level_label.setText(f'{ " " * 4} Level: {self.level} { " " * 15}  Winning Streak: {self.winning_streak} ')
            reset_timer = QTimer(self)
            reset_timer.singleShot(1000, lambda: self.show_for_help(sender))

    def resetGame(self):

        if self.winning_streak > 3:
            self.level += 1
            self.level_label.setText(f'{ " " * 4} Level: {self.level} { " " * 15}  Winning Streak: {self.winning_streak} ')
            if self.level > 999:
                self.level = 999
        for button in self.buttons:
            button.setDisabled(False)
        level = self.level
        self.start_from = 1
        self.createButtons(level)


    def show_for_help(self, button):
        button.setStyleSheet("QPushButton { background-color: black; border: 1px solid black; }")
        button.setText(button.text())

    def hideButtonColors(self):
        for button in self.buttons:
            button.setStyleSheet("QPushButton { background-color: black; border: 1px solid black; }")

def main():
    app = QApplication(sys.argv)
    game = MemoryGame()
    game.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
